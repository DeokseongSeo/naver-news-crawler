# import modules
import datetime
import os
import json
from utils import *
from konlpy.tag import Twitter
import re
import glob
import shutil
import time
from Config import *



class Crawler:

    def __init__(self):
        self.sid = config['SID']
        self.cate_dict = {100: '정치', 101: '경제', 102: '사회', 103: '문화', 104: '세계', 105: 'IT_과학', 110: '오피니언'}
        # cate_dict = {100: 'politics', 101: 'economy', 102: 'society', 103: 'culture', 104: 'world', 105: 'IT_Science', 110: 'opinion'}
        self.numdays = config['NUMDAYS']
        self.hour = 0
        self.update_time = config['UPDATE_TIME']
        self.out_dir = config['OUT_DIR']
        self.json_folder = self.out_dir + '/jsons'
        self.t = Twitter()
        self.stop_POS = ['Josa', 'Eomi', 'Foreign', 'Punctuation', 'Suffix', 'PreEomi', 'VerbPrefix', 'Number']
        if os.getcwd() + '/data' not in glob.glob(os.getcwd() + '/*'):
            os.makedirs(os.getcwd() + '/data/jsons')


    # 이전 날짜 지우고, 새 날짜 준비
    def check_days(self):
        folders = glob.glob(self.json_folder + '/*')
        day_folders = []
        for i, x in enumerate(folders):
            dayday = re.split('/', x)[-1]
            day_folders.append(dayday)

        now_days = set([re.findall('[0-9]{8}', x)[0] for x in day_folders])
        today = datetime.datetime.today()
        date_list = set([(today - datetime.timedelta(days=x)).strftime('%Y%m%d') for x in range(0, self.numdays)])

        # json 지우는 부분
        self.delete_days = list(now_days.difference(date_list))
        for del_day in self.delete_days:
            shutil.rmtree(self.json_folder+'/'+del_day)
            print("{} is deleted".format(del_day))

        self.crawling_day = sorted(list(date_list.difference(now_days)))

    # 크롤링
    def crawl(self):

        self.check_days()
        print('crawling start')
        print(self.crawling_day)
        # self.check_hour()
        # hour = datetime.datetime.now().strftime('%H')

        # if int(hour) == self.update_time:

        for sid_num in self.sid:
            stack = ['a'] * 20
            # 속보에서, 100: 정치, 101: 경제, 102: 사회, 103: 생활/문화, 104: 세계, 105: IT/과학, 110: 오피니언
            cate_name = self.cate_dict[sid_num]

            before_time = datetime.datetime.today()

            for ind, date in enumerate(self.crawling_day):
                if date not in os.listdir(self.json_folder):
                    os.makedirs(self.json_folder+'/'+date)

                page = 1
                time.sleep(2)
                news_ind = 0
                while True:
                    print(
                        'sid1: {sid1} || day: {day} || page: {page} ----'.format(sid1=sid_num,
                                                                                 day=date,
                                                                                 page=page))
                    list_URL = 'http://news.naver.com/main/list.nhn?mode=LSD&mid=sec&' \
                               'sid1={sid1}&date={day}&page={page}'.format(sid1=sid_num, day=date, page=page)
                    urls = get_url(list_URL)
                    num = len(urls)

                    result_title, result_text = get_text(urls)
                    output = np.c_[urls, result_title, result_text]

                    if num == 20:
                        if all(output[:, 1] == stack):
                            break
                        else:
                            for row in output:
                                news_ind += 1
                                word_len, text2, filtered_sentence = self.filtering(row[2])
                                if len(re.sub('[a-zA-Z0-9\. ]', '', ' '.join(text2.split('<ZQQ>')[:2]))) < 6:  # 2번째 문단까지 확인. 한글 뉴스의 경우 적어도 80이상
                                    print('English news pass: {}'.format(text2))
                                    continue
                                p_date = "{}-{}-{}".format(date[0:4], date[4:6], date[6:8])

                                if word_len > 50:
                                    out = {'url': row[0],
                                           'title': clean_title(row[1]),
                                           'text': text2,
                                           'publication_date': p_date,
                                           'filtered_sentence': filtered_sentence,
                                           'category': cate_name}

                                    with open(self.json_folder+'/'+date+'/'+'{}_{}.json'.format(sid_num, news_ind), 'w', encoding='utf-8') as output_json:
                                        json.dump(out, output_json)


                            stack = output[:, 1]
                            page += 1
                    else:
                        for row in output:
                            news_ind += 1
                            word_len, text2, filtered_sentence = self.filtering(row[2])
                            if len(re.sub('[a-zA-Z0-9\. ]', '', ' '.join(text2.split('<ZQQ>')[:2]))) < 10:  # 2번째 문단까지 확인. 한글 뉴스의 경우 적어도 80이상
                                print('English news pass: {}'.format(text2))
                                continue
                            p_date = "{}-{}-{}".format(date[0:4], date[4:6], date[6:8])

                            if word_len > 50:
                                out = {'url': row[0],
                                       'title': clean_title(row[1]),
                                       'text': text2,
                                       'publication_date': p_date,
                                       'filtered_sentence': filtered_sentence,
                                       'category': cate_name}

                                with open(self.json_folder + '/' + date + '/' + '{}_{}.json'.format(sid_num, news_ind), 'w', encoding='utf-8') as output_json:
                                    json.dump(out, output_json)

                        break

            after_time = datetime.datetime.today()
            print("크롤링 소요시간: {}".format(after_time - before_time))
        # else:
        #     return

    # crawling한 json에 대해 filtering하는 함수
    def filtering(self, text):
        text1 = clean_text(text)
        text2 = phrase(text1)
        word_len, filtered_sentence = news_after_pos(text2, self.t, self.stop_POS)
        return word_len, text2, filtered_sentence


# ### Crawl naver news
# #3일치 크롤링할 경우 5시간정도 소요됨, 104(세계)는 영문 기사가 너무 많아서 제외
# cae = Crawler()
# cae.crawl()
