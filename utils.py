import re
from operator import itemgetter
import numpy as np
from bs4 import BeautifulSoup
import urllib


# 크롤링 함수
def get_text(urls):
    TITLE = []
    CONTENT = []
    for ind, URL in enumerate(urls):
        # print(ind)

        source_code_from_URL = urllib.request.urlopen(URL)
        soup = BeautifulSoup(source_code_from_URL, 'lxml')

        try:
            content = str(soup.find_all('div', id='articleBodyContents')[0])

            title_tmp = soup.find('h3', id='articleTitle')
            title = title_tmp.find_all(text=True)[0]

            TITLE.append(title)
            CONTENT.append(content)
        except:
            TITLE.append('None')
            CONTENT.append('None')
            # print('아주 경미한 에러')

    return TITLE, CONTENT


# 리스트 뽑아오는 함수
def get_url(list_URL):
    source_code_list = urllib.request.urlopen(list_URL)
    soup = BeautifulSoup(source_code_list, 'lxml')
    content = soup.find('div', id='main_content')
    content = content.find_all('li')

    urls = []
    for ind, li in enumerate(content):
        if li.find('a', class_='nclicks(fls.list)'):
            urls.append(li.find('a', class_='nclicks(fls.list)')['href'])
        # else:
        #     print('no', ind)

    return urls


# cleaning
def clean_title(title):
    cleaned_title = re.sub('\(.*?\)', '', title)
    cleaned_title = re.sub('\[.*?\]', '', cleaned_title)
    cleaned_title = re.sub('\{.*?\}', '', cleaned_title)
    cleaned_title = re.sub('\<.*?\>', '', cleaned_title)
    cleaned_title = re.sub('[!@#$%^&*`]', '', cleaned_title)
    cleaned_title = cleaned_title.strip()
    return cleaned_title


# cleaning
def clean_text(text):
    try:
        cleaned_text = re.split('\{\}\\n</script>', text)[1]
    except:
        return 'None'
    try:
        cleaned_text = re.sub('<table.*<\/table>', ' ', cleaned_text)
    except:
        cleaned_text = cleaned_text
    cleaned_text = re.sub('\xa0', ' ', cleaned_text)
    cleaned_text = re.sub('<br/>', '\\n', cleaned_text)
    cleaned_text = re.sub('<br>', '\\n', cleaned_text)
    cleaned_text = re.sub('<em .*?</em>', '', cleaned_text)
    cleaned_text = re.sub('<caption>.*?</caption>', '', cleaned_text)
    cleaned_text = re.sub('<[a-zA-Z0-9 \{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$&\\\=\(\'\"]+?>', '', cleaned_text)  # %살림
    # cleaned_text = re.sub('\\n', ' ', cleaned_text)
    # cleaned_text = re.sub('\\t', ' ', cleaned_text)
    cleaned_text = re.split('\n?[a-zA-Z0-9]*@[a-zA-Z0-9]*\.[a-zA-Z0-9]*', cleaned_text)[0]
    cleaned_text = re.sub('[.* [가-힣 ]+?]', '', cleaned_text)
    cleaned_text = re.sub('\(.*?\)', '', cleaned_text)
    # cleaned_text = re.sub('\/*[가-힣 ]{4}?기자', '', cleaned_text)
    cleaned_text = re.sub('[\{\}\/?,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\']', '', cleaned_text)
    cleaned_text = re.sub(' {2,}', ' ', cleaned_text)
    cleaned_text = cleaned_text.strip()
    return cleaned_text


# 문단 구분
def phrase(text):
    cleaned_text = re.sub('\\n\\n\\n\\n', '<ZQQ>', text)
    cleaned_text = re.sub('\\n\\n\\n', '<ZQQ>', cleaned_text)
    cleaned_text = re.sub('\\n\\n', '<ZQQ>', cleaned_text)
    cleaned_text = re.sub('\\n', ' ', cleaned_text)
    return cleaned_text


# POS 이후 결과물 산출
def news_after_pos(x, t, stop_POS):
    POS = t.pos(x, norm=True, stem=True)
    text_pos = list(map(itemgetter(1), POS))
    word_len = len(text_pos) - sum(np.in1d(text_pos, stop_POS))

    filtered_word1 = np.array(POS)[np.logical_not(np.in1d(text_pos, stop_POS))]
    filtered_word2 = list(map(itemgetter(0), filtered_word1))
    filtered_word3 = sum([re.findall('[a-zA-Z가-힣]{2,}', word) for word in filtered_word2], [])
    filtered_sentence = ' '.join(filtered_word3)

    return word_len, filtered_sentence


# POS 이후 결과물 산출 news extract 용도
def query_split(x, t, stop_POS):
    stop_words = ['뉴스', '관련', '알다', '검색', '해주다', '언제', '이슈', '정보']

    POS = t.pos(x, norm=True, stem=True)
    text_pos = list(map(itemgetter(1), POS))

    filtered_word1 = np.array(POS)[np.logical_not(np.in1d(text_pos, stop_POS))]
    filtered_word2 = list(map(itemgetter(0), filtered_word1))
    filtered_word3 = sum([re.findall('[a-zA-Z가-힣]{2,}', word) for word in filtered_word2], [])
    filtered_word4 = list(np.array(filtered_word3)[np.logical_not(np.in1d(filtered_word3, stop_words))])

    return filtered_word4


# word index 가져오는 함수
def get_id(x, dictionary):
    try:
        return dictionary.token2id[x]
    except:
        return


def get_token(x, dictionary):
    try:
        return dictionary[x]
    except:
        return
