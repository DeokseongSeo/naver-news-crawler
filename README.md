# Naver news crawler


- 푸딩, 로보혼에서 사용한 네이버 뉴스 크롤러 upload합니다.
 
- 전체 7개 뉴스 속보 카테고리(정치, 경제, 사회, 문화, 세계, IT_과학, 오피니언) 중, 영문 기사가 많은 세계 카테고리를 제외한 6개 카테고리에 대해 자동으로 크롤링 됩니다. 
(세계 기사가 필요하신 분께서는 Config.py에서 SID변수에 104를 추가해주시면 됩니다.)
 
- Config.py에서 NUMDAYS는 오늘 날짜 기준으로 며칠의 기사를 크롤링 할지를 의미합니다. 이부분을 필요하신 만큼 수정하셔서 사용하시면 될 듯 합니다. (2018년에 지원받은 고성능 맥북 기준, 3일치 크롤링시 5시간 소요되었습니다.)
 
- Config.py를 필요에 맞게 수정하신 다음 main_code.py를 실행시키시면 자동으로 해당 폴더 내 data 디렉토리가 생성되고, 날짜별, 카테고리별로 뉴스 크롤링이 진행됩니다. 뉴스는 105_23.json과 같이 저장되는데, 이는 105 카테고리의 23번째 뉴스임을 의미합니다. (뉴스 index인 23은 큰 의미가 없습니다.)
 
- 감사합니다.